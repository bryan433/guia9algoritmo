#include <iostream>

using namespace std;

#ifndef COLISION_INGRESO_H
#define COLISION_INGRESO_H

class Colisioningreso{
	
	private:
	
	public:
		Colisioningreso();
		
		void imprimir(int *arreglo, int tamano);
        void colisionpruebalineal(int posicion, int *arreglo, int tamano, int num);
        void colisionPruebaCuadratica(int posicion, int *arreglo, int tamano, int num);
        void colisionDobleDireccion(int posicion, int *arreglo, int tamano, int num);

};

#endif
