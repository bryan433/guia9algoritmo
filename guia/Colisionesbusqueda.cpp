#include <iostream>
#include "Colisionesbusqueda.h"

using namespace std;

Colisionesbusqueda::Colisionesbusqueda(){
}

void Colisionesbusqueda::imprimir(int *arreglo, int tamano){
	for (int i = 0; i < tamano; i++){
		cout << i << " ["  << arreglo[i] << "]" << endl;
	}
}


void Colisionesbusqueda::colisionespruebalineal(int posicion, int *arreglo, int tamano, int num){
	int nuevaposicion;
	
	if (arreglo[posicion] != -1 && arreglo[posicion] == num){
		cout << num << " se encuentra en la posicion" << posicion << endl;
	}
	
	else{
		nuevaposicion = posicion + 1;
		while (arreglo[nuevaposicion] != -1 && nuevaposicion <= tamano && nuevaposicion != posicion && arreglo[nuevaposicion] != num){
			nuevaposicion = nuevaposicion + 1;
			if (nuevaposicion == tamano + 1){
				nuevaposicion = 0;
			} 
		}
		if (arreglo[nuevaposicion] == -1 || nuevaposicion == posicion){
			cout << num << " no se encuentra en el arreglo" << endl;
			imprimir(arreglo, tamano);
		}
		else{
			cout << num << " se encuentra en la posicion" << nuevaposicion << endl;
			imprimir(arreglo, tamano);
		}
	}
}


void Colisionesbusqueda::colisionesPruebaCuadratica(int posicion, int *arreglo, int tamano, int num){
    int i;
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ 
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        i = 1;
        nuevaPosicion = (posicion + (i*i)); 
        while(arreglo[nuevaPosicion] != -1 && arreglo[nuevaPosicion] != num){
            i = i + 1; 
            nuevaPosicion = (posicion + (i*i));

            if(nuevaPosicion > tamano){ 
                i = 1;
                nuevaPosicion = 0; 
                posicion = 0;
            }
        }

        if(arreglo[nuevaPosicion] == -1){
            cout << num << " no se encuentra en el arreglo" << endl;
            imprimir(arreglo, tamano);
        }

	    else{
            cout << num << " se encuentra en la posición " << nuevaPosicion << endl;
            imprimir(arreglo, tamano);
        }
    }
}


void Colisionesbusqueda::colisionesDobleDireccion(int posicion, int *arreglo, int tamano, int num){
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ 
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1;
        while(nuevaPosicion <= tamano && arreglo[nuevaPosicion] != -1 && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano -1) + 1; 
        }

        if(arreglo[nuevaPosicion] == -1 || (nuevaPosicion == posicion)){ 
            cout << num << " no se encuentra en el arreglo" << endl;
            imprimir(arreglo, tamano);
        }

	    else{
            cout << num << " se encuentra en la posición " << nuevaPosicion << endl;
            imprimir(arreglo, tamano);
        }
    }
}
