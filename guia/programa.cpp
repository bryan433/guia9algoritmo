#include <iostream>
// Libreria para poder utilizar strcmp
#include <string.h>
// Definición de la clase
#include "Colisioningreso.h"
#include "Colisionesbusqueda.h" 
#include "Cadena.h"

using namespace std;

void imprimir (int *arreglo, int tamano){
	for (int i = 0; i < tamano; i++){
		cout << i << " [" << arreglo[i] << "]" << endl;
	}
}


void arreglovacio(int *arreglo, int tamano){
	for (int i = 0; i < tamano; i++){
		arreglo[i] = -1;
	}
}


void metodoColisionesInsercion(int *arreglo, int tamano, int num, char *metodo, int posicion){
    Colisioningreso colision = Colisioningreso();
	
	// strcmp permite comparar un puntero, cero significa que son iguales
    if(strcmp(metodo, "L") == 0){ 
        colision.colisionpruebalineal(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "C") == 0){
        colision.colisionPruebaCuadratica(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "D") == 0){
        colision.colisionDobleDireccion(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "E") == 0 ){
        Cadena cadena = Cadena();
        cadena.metodoencadenamiento(arreglo, tamano, num, posicion);
    }
	else{
		system("clear");
		cout << "Reinicie el programa" << endl;
		cout << "debe ignresar algun parametro" << endl;
	}
}


void metodoColisionesBusqueda(int *arreglo, int tamano, int num, char *metodo, int posicion){
    Colisionesbusqueda colision = Colisionesbusqueda();
    
    if(strcmp(metodo, "L") == 0){
        colision.colisionespruebalineal(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "C") == 0){
        colision.colisionesPruebaCuadratica(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "D") == 0){
        colision.colisionesDobleDireccion(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "E") == 0 ){
        Cadena cadena = Cadena();
        cadena.metodoencadenamiento(arreglo, tamano, num, posicion);
    }

    else{
        system("clear");
        cout << "Reinicie el programa " << endl;
        cout << "Debe ingresar algun parametro" << endl;
        
    }   
}


// Determinara la posición donde se agregara el dato
void funcionHash(int *arreglo, int tamano, int num, char *metodo){ 
    int posicion;
    
    // Función hash por módulo, asigna una posición para guardar el dato
    posicion = (num%(tamano - 1)) + 1; 
    
    // Hay una colisión
    if(arreglo[posicion] != -1){ 
        cout << "Colision en posicion " << posicion << endl;
        metodoColisionesInsercion(arreglo, tamano, num, metodo, posicion);
    }

    else{
        arreglo[posicion] = num;
        imprimir(arreglo, tamano);
    }
}


bool arregloLleno(int *arreglo, int tamano){
    int contador;

    for(int i = 0; i < tamano; i++){
        if(arreglo[i] != -1){
            contador++;
        }
    }

    if(contador <= tamano && contador > 0){
        return true;
    }

    else{
        return false;
    }
}


int pedirNumero(){
    int num;

    system("clear");
    cout << "Ingrese el número" << endl;
    cin >> num;

    return num;
}


int menu (char *metodo, int tamano){
	int num;
	int opcion;
	int posicion;
	int arreglo[tamano];
	bool flag;
	arreglovacio(arreglo, tamano);
	
	while (opcion != 3){
		cout << "Ingrese una opcion: " << endl;
		cout << "[1] Ingresar numeros" << endl;
		cout << "[2] Buscar un numero" << endl;
		cout << "[3] Salir" << endl;
		
		cin >> opcion;
		
		switch(opcion){
			case 1:
				num = pedirNumero();
				funcionHash(arreglo, tamano, num, metodo);
				break;
			
			case 2:
				flag = arregloLleno(arreglo, tamano);
				if (flag == true){
					num = pedirNumero();
					
					posicion = (num%(tamano - 1)) + 1;
					
					system("clear");
					
					if (arreglo[posicion] == num){
						cout << num << " se encuentra en la posicion " << posicion << endl;
						imprimir(arreglo, tamano);
					}
					else{
						cout << "Colision en posicion " << posicion << endl;
						metodoColisionesBusqueda(arreglo, tamano, num, metodo, posicion);
					}
				}
				
				break;
				
			case 3:
				break;
			
			default:
				system("clear");
				cout << "intentelo denuevo" << endl;
				break;
		}
	}
}


int validarNumeroParametros(int argc, char *argv[], int tamano ){
    if(argc == 2){ 
        menu(argv[1], tamano);
    }
    
    else{
        cout << "Reinicie el programa y ingrese los parametros correctos" << endl;
        return -1;
    }
}


int main(int argc, char *argv[]){
    int parametro;
    int tamano;

    cout << "Defina el tamaño del arreglo: ";
    cin >> tamano;
    
    system("clear");
    parametro = validarNumeroParametros(argc, argv, tamano);
}



    




