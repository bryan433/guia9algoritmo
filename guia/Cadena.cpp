#include <iostream>
#include "Cadena.h"

using namespace std;

Cadena::Cadena(){

}


void Cadena::copiar(nodo **arreglo1, int *arreglo, int tamano){
	for (int i = 0; i < tamano; i++){
		arreglo1[i]->numero = arreglo[i];
		arreglo1[i]->sig = NULL;
	}
}


void Cadena::imprimir (nodo **arreglo, int posicion){
	nodo *aux = NULL;
	aux = arreglo[posicion];
	
	while (aux != NULL){
		if (aux->numero != 0){
			cout << "[" << aux->numero << "] -" << endl;
		}
		
		aux = aux->sig;
	}
}


void Cadena::metodoencadenamiento(int *arreglo, int tamano, int num, int posicion){
	nodo *temporal = NULL;
	nodo * arregloAux[tamano];
	
	for (int i =0; i < tamano; i++){
		arregloAux[i] = new nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->numero = -1;
	}
	
	copiar(arregloAux, arreglo, tamano);
	
	if (arreglo[posicion] != -1 && arreglo[posicion] == num){
		cout << num << " esta en la posicion" << posicion << endl;
	}
	else{
		temporal = arregloAux[posicion]->sig;
		
		while (temporal != NULL && temporal->numero != num){
			temporal = temporal->sig;
		}
		
		if (temporal == NULL){
			cout << num << " no se encuentra en la lista" << endl;
			cout << " lista" << endl;
			imprimir(arregloAux, posicion);
		}
	}
}
