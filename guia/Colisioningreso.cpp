#include <iostream>
#include "Colisioningreso.h"

using namespace std;

Colisioningreso::Colisioningreso(){

}


void Colisioningreso::imprimir(int *arreglo, int tamano){
	for (int i = 0; i < tamano; i++){
		cout << i << " ["  << arreglo[i] << "]" << endl;
	}
}


void Colisioningreso::colisionpruebalineal(int posicion, int *arreglo, int tamano, int num){
	int nuevaposicion;
	int contador;
	
	if (arreglo [posicion] != -1 && arreglo[posicion] == num){
		cout << num << " se encuentra en la posicion" << posicion << endl;
	}
	else{
		contador = 0;
		nuevaposicion = posicion + 1;
		
		while (arreglo[nuevaposicion] != -1 && nuevaposicion <= tamano && nuevaposicion != posicion && arreglo[nuevaposicion] != num){
			nuevaposicion = nuevaposicion + 1;
			if (nuevaposicion == tamano + 1){
				nuevaposicion = 0;
			}
			contador = contador + 1;
		}
		
		if (contador == tamano){
			cout << "No quedan espacios en el arreglo" << endl;
		}
		
		if (arreglo[nuevaposicion] == -1){
			arreglo[nuevaposicion] = num;
			cout << num << " se movio a la posicion " << nuevaposicion << "\n" << endl;
			imprimir(arreglo, tamano);
		}
	}
}


void Colisioningreso::colisionPruebaCuadratica(int posicion, int *arreglo, int tamano, int num){
	int i;
	int nuevaposicion;
	
	if (arreglo[posicion] != -1 && arreglo[posicion] == num){
		cout << num << " se encuentra en la posicion " << posicion << endl;
	}
	
	else{
		i = 1;
		nuevaposicion = (posicion + (i*i));
		while (arreglo[nuevaposicion] != -1 && arreglo [nuevaposicion] != num){
			i = i + 1;
			nuevaposicion = (posicion + (i*i));
			
			if (nuevaposicion < tamano){
				i = 1;
				nuevaposicion = 0;
				posicion = 0;
			}
		}
		
		if (arreglo[nuevaposicion] == -1){
			arreglo[nuevaposicion] = num;
			cout << num << " se movio la posicion " << nuevaposicion << "\n" << endl;
			imprimir(arreglo, tamano);
		}
	}
}


void Colisioningreso::colisionDobleDireccion(int posicion, int *arreglo, int tamano, int num){ 
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ 
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1; 
  
        while(nuevaPosicion <= tamano && arreglo[nuevaPosicion] != -1 && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano - 1) + 1;
        }
        
        if(arreglo[nuevaPosicion] == -1){ 
            arreglo[nuevaPosicion] = num;
            if(nuevaPosicion == tamano + 1){
                cout << "No quedan espacios en el arreglo" << endl;
            }

            else{
                cout << num << " se movio a la posición " << nuevaPosicion << "\n" << endl;
                imprimir(arreglo, tamano);
            }
        }
    }
}
