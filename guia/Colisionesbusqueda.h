#include <iostream>

using namespace std;

#ifndef COLISIONES_BUSQUEDA_H
#define COLISIONES_BUSQUEDA_H

class Colisionesbusqueda{

	private:
	
	public:
		Colisionesbusqueda();
		
		void imprimir(int *arreglo, int tamano);
		void colisionespruebalineal(int posicion, int *arreglo, int tamano, int num);
		void colisionesDobleDireccion(int posicion, int *arreglo, int tamano, int num);
		void colisionesPruebaCuadratica(int posicion, int *arreglo, int tamano, int num);
};
#endif 
