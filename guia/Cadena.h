#include <iostream>

using namespace std;

typedef struct _nodo{
		int numero;
		struct _nodo *sig;
} nodo;


class Cadena{
	
	private:
	
	public:
		Cadena();
		void copiar(nodo **arreglo1, int *arreglo, int tamano);
		void imprimir (nodo **arreglo, int posicion);
		void metodoencadenamiento(int *arreglo, int tamano, int num, int posicion);
};
